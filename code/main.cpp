//
//  main.cpp
//
//  Created by
//      Francisco Ramos  157330
//      Aldair González  158020   
//      Mariella Sánchez 157130
//      Karla Martínez   156758
//  on 13/03/21.

#include <iostream>
#include <omp.h>

#include "Image.cpp"

using namespace std;


// g++ -o main main.cpp
int main(int argc, char *argv[]) {

    /* Set the selected size of the image */
    string size = argv[1];

    /* Set the times to run the convolution */
    int iterations = atoi(argv[2]);

    /* Set the number of threads */
    int threads = omp_get_max_threads();

    // cout << endl;
    // cout << "Runing the convolution\n";
    cout << "Image size: " << size << endl;
    cout << "Number of iterations: " << iterations << endl;
    cout << "Number of threads: " << threads << endl;

    /*-------------------------------------------------------------------------------------------*/
    
    /* Set the paths for loading and saving the images */
    string loadPath = "../images/";
    string savePath = "../results/";
    
    /* Set the name of the images */
    string loadNames[3] = {
        "lena.jpg",
        "lena2x.jpg",
        "lena3x.jpg"
    };

    /* Set the image of the result images */
    string saveNames[3] = {
        "lena_result",
        "lena2x_result",
        "lena3x_result"
    };

    /* Set the default image  */
    string loadName = loadNames[0];
    string saveName = saveNames[0];

    if(size == "small"){
        loadName = loadNames[0];
        saveName = saveNames[0];
    }
    if(size == "medium"){
        loadName = loadNames[1];
        saveName = saveNames[1];
    }

    if(size == "large"){
        loadName = loadNames[2];
        saveName = saveNames[2];
    }

    /* Create the images objects with the paths to the images */
    Image lena  = Image( (loadPath + loadName).c_str() );

    /* Defining kernel (Box blur) */
    double kernel[3][3] = {
        {0.111, 0.111, 0.111},
        {0.111, 0.111, 0.111},
        {0.111, 0.111, 0.111},
    };
    
//    double kernel[3][3] = {
//        {-1, -1, -1},
//        {-1,  8, -1},
//        {-1, -1, -1},
//    };
    

    /* Runing the times selected */
    double normalSum   = 0;
    double parallelSum = 0;
    for (int i = 0; i < iterations; i++){
        // cout << "Run " << i << ":\n";

        /* Making the normal convolution */
        normalSum   += lena.normalConvolution(kernel);

        lena.revertMatrixToOriginalImg();

        /* Making the parallel convolution */
        parallelSum += lena.parallelConvolution(kernel);

        // cout << endl;
    }
    cout << "Average time for normal convolution after "   << iterations << " iterations: " << (normalSum/iterations)   << "s\n";
    cout << "Average time for parallel convolution after " << iterations << " iterations: " << (parallelSum/iterations) << "s\n";
    cout << endl;
    

    return 0;
}

