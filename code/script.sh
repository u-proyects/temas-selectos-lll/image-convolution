#!/bin/bash
#convolution 

#compile
g++ -fopenmp -o convolution main.cpp

#run
echo -e "\nRuning the convolution\n"

#size: small
#threads: 1
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=1
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution small 10

#size: medium
#threads: 1
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=1
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution medium 10

#size: large
#threads: 1
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=1
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution large 10

echo "---------------------"

#size: small
#threads: 5
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=5
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution small 10

#size: medium
#threads: 5
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=5
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution medium 10

#size: large
#threads: 5
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=5
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution large 10

echo "---------------------"

#size: small
#threads: 10
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=10
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution small 10

#size: medium
#threads: 10
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=10
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution medium 10

#size: large
#threads: 10
#iterations: 10
#schedule type: static
export OMP_NUM_THREADS=10
export OMP_SCHEDULE=static
echo "Schedule type: static"
./convolution large 10

echo "---------------------"

#size: small
#threads: 10
#iterations: 10
#schedule type: dynamic
export OMP_NUM_THREADS=10
export OMP_SCHEDULE=dynamic
echo "Schedule type: dynamic"
./convolution small 10

#size: medium
#threads: 10
#iterations: 10
#schedule type: dynamic
export OMP_NUM_THREADS=10
export OMP_SCHEDULE=dynamic
echo "Schedule type: dynamic"
./convolution medium 10


#size: large
#threads: 10
#iterations: 10
#schedule type: dynamic
export OMP_NUM_THREADS=10
export OMP_SCHEDULE=dynamic
echo "Schedule type: dynamic"
./convolution large 10

